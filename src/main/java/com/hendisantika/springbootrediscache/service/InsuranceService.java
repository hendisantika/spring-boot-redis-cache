package com.hendisantika.springbootrediscache.service;

import com.hendisantika.springbootrediscache.domain.Insurance;
import com.hendisantika.springbootrediscache.repository.InsuranceRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-redis-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/12/19
 * Time: 19.29
 */
@Description(value = "Insurance service responsible for processing data.")
@Service
public class InsuranceService {

    private InsuranceRepository insuranceRepository;

    @PersistenceContext
    private EntityManager entityManager;


    /**
     * Constructor dependency injector.
     *
     * @param insuranceRepository - insurance repository layer.
     */
    public InsuranceService(InsuranceRepository insuranceRepository) {
        this.insuranceRepository = insuranceRepository;
    }

    /**
     * Method for getting all insurances
     *
     * @param county - provided county name
     * @return List of insurances / cacheable values
     */
    @Cacheable(value = "insurances", key = "#county")
    public List<Insurance> findAllInsurancesByCounty(String county) {
        return insuranceRepository.findByCounty(county);
    }

    /**
     * Method for getting insurance by identifier
     *
     * @param id - insurance identifier
     * @return Insurance / cacheable value
     */
    @Cacheable(value = "insurance", key = "#id")
    public Insurance findById(Integer id) {
        return insuranceRepository.findById(id).get();
    }

    /**
     * Method for storing new insurance and caching it in redis
     *
     * @param insurance - insurance payload
     * @return stored Insurance
     */
    @CachePut(value = "insurance", key = "#insurance.id")
    public Insurance store(Insurance insurance) {
        return insuranceRepository.save(insurance);
    }

    /**
     * Method for destroying insurance by identifier and remove it from redis
     *
     * @param id - insurance identifier
     */
    @CacheEvict(value = "insurance", key = "#id")
    public void destroy(Integer id) {
        insuranceRepository.deleteById(id);
    }
}
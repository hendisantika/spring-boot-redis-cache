package com.hendisantika.springbootrediscache.repository;

import com.hendisantika.springbootrediscache.domain.Insurance;
import org.springframework.context.annotation.Description;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-redis-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/12/19
 * Time: 19.28
 */
@Description(value = "Insurance Repository Layer.")
@Repository
public interface InsuranceRepository extends JpaRepository<Insurance, Integer> {

    List<Insurance> findByCounty(String county);

}